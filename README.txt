This is a simple module that provides a text filter designed to convert
telephone numbers in text into clickable links using the following format

```
<a href="tel:5555555555">555-555-5555</a>
```

The module uses a regular expression (regex) that works on most
U.S./Canadian style phone numbers.

If your text looks like this:

  Call 555-555-5555 for details.

This module will convert it into this:

```
  Call <a href="tel:5555555555">555-555-5555</a> for details.
```

The regular expression can be configured by the user via the module's
settings page which can be found at:

/admin/config/content/telephone-filter
