<?php
/**
* @file
* Defines the administration settings form and other adminstrative functions
* for telephone_filter.
*/

/**
* Form generation function telephone filter regex configuration.
*/
function telephone_filter_admin_settings_form() {
  $form = array();

  $form['title'] = array(
    '#markup' => t('By default telephone filter module uses the following regex to determine if a string is a telephone number.'),
  );

  $form['telephone_filter_custom'] = array(
    '#type' => 'radios',
    '#title' => t('Customize regex?'),
    '#options' => array(
      0 => 'Use default',
      1 => 'Custom override',
    ),
    '#default_value' => variable_get('telephone_filter_custom', 0),
    '#required' => TRUE,
    '#description' => t('You can choose to override the default regex to meet your own criteria if you find that the telephone filter is not behaving the way you want in all scenarios.'),
  );

  $form['telephone_filter_regex'] = array(
    '#type' => 'textfield',
    '#title' => t('Regex'),
    '#default_value' => variable_get('telephone_filter_regex', TELEPHONE_FILTER_DEFAULT_REGEX),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="telephone_filter_custom"]' => array('value' => 1),
      ),
    ),
  );

  $test_text = '(555-555-5555)<br>
                  55555555555<br>
                  1,555,555,555<br>
                  (555)555-5555<br>
                  (555)555 5555<br>
                  (555) 5555555.<br>
                  555-555-5555<br>
                  555 555 5555<br>
                  555.555.5555<br>
                  555 555-5555<br>
                  555555-5555-555<br>
                  1-(555) 555-5555 test<br>
                  1-(555)555-5555 test<br>
                  1-(555)555 5555<br>
                  1-(555) 5555555<br>
                  1-(555)5555555<br>
                  1-(555)-555-5555<br>
                  1-555 555 5555<br>
                  1-555.555.5555<br>
                  1-555 555-5555<br>
                  555555-5555-555 test<br>
                  1-555-555-5555<br><br>';

  $form['telephone_filter_test'] = array(
    '#prefix' => '<div style="float:left; width: 40%;"><h3>' . t('Example Input') . '</h3>',
    '#suffix' => '</div>',
    '#markup' => $test_text,
    '#weight' => 200,
  );

  $form['telephone_filter_test_filtered'] = array(
    '#prefix' => '<div style="float:left; width: 40%;"><h3>' . t('Example Output') . '</h3>',
    '#suffix' => '</div>',
    '#markup' => telephone_filter_filter_process($test_text, NULL, NULL, NULL, NULL, NULL),
    '#weight' => 201,
  );

  return system_settings_form($form);
}

/**
 * Validation handler for telephone_filter_admin_settings_form.
 */
function telephone_filter_admin_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['telephone_filter_custom']) {
    $regex = $form_state['values']['telephone_filter_regex'];

    // TODO Currently there appears to be no way to detect a valid regex without
    // throwing a warning if it is bad.
    if (@preg_match($regex, NULL) === FALSE) {
      form_set_error('telephone_filter_regex', 'Invalid regular expresion');
    }
  }
}

